import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.21" apply false
    id("com.github.johnrengelman.shadow") version "5.2.0" apply false
}

subprojects {
    group = "name.mlopatkin.ijavac"
    version = "0.1-SNAPSHOT"

    apply(plugin = "org.jetbrains.kotlin.jvm")
    repositories {
        mavenCentral()
    }

    dependencies {
        "implementation"(kotlin("stdlib-jdk8"))
        "implementation"(Deps.slf4j.api)

        "testImplementation"(platform(Deps.junit.bom))
        "testImplementation"(Deps.junit.jupiter)
        "testImplementation"(Deps.assertj)
    }

    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }

    tasks.withType<Test>().configureEach {
        useJUnitPlatform()
    }

    project.afterEvaluate {
        if (plugins.findPlugin("application") != null) {
            project.configure<ApplicationPluginConvention> {
                applicationDefaultJvmArgs = arrayListOf("-Dfile.encoding=UTF-8")
            }
        }
    }
}

// Add a meta "dist" targets that collects dist archives of all application subprojects.
tasks.register<Copy>("dist").configure {
    destinationDir = project.buildDir.resolve("dist")
    val sources = ArrayList<File>()
    val copyTask = this
    rootProject.subprojects.forEach { project ->
        try {
            val distZip = project.tasks.named("distZip", Zip::class)
            copyTask.dependsOn(distZip)
            sources.add(distZip.get().archiveFile.get().asFile)
        } catch (ignored: UnknownTaskException) {
        }
    }
    from(sources)
}

tasks.register("clean").configure {
    doLast {
        rootProject.buildDir.deleteRecursively()
    }
}
