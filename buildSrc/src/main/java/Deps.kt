object Deps {
    const val picocli = "info.picocli:picocli:4.6.1"
    object slf4j {
        private const val SLF4J_VERSION = "1.7.30"

        const val api = "org.slf4j:slf4j-api:$SLF4J_VERSION"
        const val simple = "org.slf4j:slf4j-simple:$SLF4J_VERSION"
    }

    object junit {
        private const val JUNIT_VERSION = "5.7.1"
        const val bom = "org.junit:junit-bom:$JUNIT_VERSION"
        const val jupiter = "org.junit.jupiter:junit-jupiter"
    }
    const val assertj = "org.assertj:assertj-core:3.19.0"
    const val guava = "com.google.guava:guava:30.1-jre"
}
