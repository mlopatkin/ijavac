plugins {
    id("application")
}

dependencies {
    implementation(project(":core"))
    implementation(project(":core-javac"))

    implementation(Deps.picocli)
    implementation(Deps.slf4j.simple)
}

application {
    applicationName = "ijavac"
    mainClass.set("name.mlopatkin.ijavac.cli.MainKt")
}
