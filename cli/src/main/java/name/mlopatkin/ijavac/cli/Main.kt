package name.mlopatkin.ijavac.cli

import name.mlopatkin.ijavac.core.IncrementalCompilationResult
import name.mlopatkin.ijavac.core.IncrementalCompiler
import name.mlopatkin.ijavac.core.SourceFile
import name.mlopatkin.ijavac.core.javac.JavacCompiler
import name.mlopatkin.ijavac.core.makeLogger
import name.mlopatkin.ijavac.core.snapshot.SnapshotFile
import picocli.CommandLine
import java.io.Closeable
import java.io.File
import java.util.Stack
import java.util.concurrent.Callable
import javax.tools.OptionChecker
import javax.tools.ToolProvider
import kotlin.system.exitProcess

private val UNSUPPORTED_OPTIONS = setOf(
    "--processor-path",
    "-processorpath",
    "-processor",
    "--processor-module-path",
)

private val UNSUPPORTED_OPTION_PREFIXES = listOf(
    "-Xplugin:"
)

@CommandLine.Command(
    name = "ijavac",
    description = ["Incremental compiler wrapper for java."],
    showAtFileInUsageHelp = true,
    footer = ["Most of javac command-line arguments are also accepted."]
)
class Main : Callable<Int>, Closeable {
    private val logger by lazy { makeLogger() }

    @CommandLine.Option(names = ["-help", "-h"], usageHelp = true, description = ["Show usage."])
    private var helpRequested = false

    @CommandLine.Option(
        names = ["-Iv", "-Iverbose"],
        description = ["Logging verbosity, specify multiple time to increase. Doesn't affect javac's verbosity."]
    )
    private var verbosity: BooleanArray = booleanArrayOf()

    @CommandLine.Option(names = ["-d"], description = ["Specify where to place generated class files."])
    private var outputDir = File(".")

    @CommandLine.Option(
        names = ["-cp", "-classpath"],
        description = ["Specify where to find user class files and annotation processors."]
    )
    private var classpath: String? = null

    @CommandLine.Parameters(preprocessor = JavacOptionProcessor::class, description = ["Source files to compile."])
    private var inputFiles: List<File> = emptyList()
    private val otherJavacOptions = ArrayList<String>()

    private var hasUnsupportedJavacOption = false

    @CommandLine.Option(
        names = ["-Isnapshot"],
        description = ["Path to the compilation info from the previous invocation. This file will be updated after " +
                "a successfull compilation."]
    )
    private var snapshotFile: File? = null

    private val javaCompiler = ToolProvider.getSystemJavaCompiler()
    private val fileManager =
        javaCompiler.getStandardFileManager(/* diagnosticListener */ null, /* locale */ null, /* charset */ null)

    override fun close() {
        fileManager.close()
    }

    override fun call(): Int {
        setupLogging()
        if (inputFiles.isEmpty()) {
            logger.error("No input files")
            return 1
        }
        JavacCompiler(javaCompiler, fileManager, classpath, otherJavacOptions).use { compiler ->
            val snapshot: SnapshotFile? = if (hasUnsupportedJavacOption) {
                logger.warn("Some javac options are unsupported. Incremental compilation disabled.")
                null
            } else {
                snapshotFile?.let { SnapshotFile(it) }
            }

            val incrementalCompiler = IncrementalCompiler(outputDir, compiler, snapshot)
            val result = incrementalCompiler.compile(inputFiles.map { file -> SourceFile(file) })

            when (result) {
                is IncrementalCompilationResult.Success -> {
                    logger.info("Recompiled {}/{}", result.recompiledInputs.size, inputFiles.size)
                }
                is IncrementalCompilationResult.NonIncrementalSuccess -> {
                    logger.warn("Non-incremental compilation performed: {}", result.reason.description)
                    if (result.additionalDescription != null) {
                        logger.info(result.additionalDescription)
                    }
                }
                is IncrementalCompilationResult.Failure -> {
                    logger.error("Compilation failed: {}", result)
                }
            }
            if (result.success) {
                return 0
            }
            return 1
        }
    }

    private fun setupLogging() {
        System.setProperty(
            "org.slf4j.simpleLogger.defaultLogLevel", when (verbosity.size) {
                0 -> "warn"
                1 -> "info"
                2 -> "debug"
                else -> "trace"
            }
        )
    }

    private class JavacOptionProcessor : CommandLine.IParameterPreprocessor {
        override fun preprocess(
            args: Stack<String>,
            commandSpec: CommandLine.Model.CommandSpec,
            argSpec: CommandLine.Model.ArgSpec?,
            info: MutableMap<String, Any>?
        ): Boolean {
            assert(argSpec != null)
            if ((commandSpec.userObject() as Main).tryConsumeToolsArgument(args)) {
                // This was a javac option (possibly with a parameter). It is now consumed, Picocli should stop.
                return true
            }
            val arg = args.peek()
            if (arg.endsWith(".java") || !arg.startsWith('-')) {
                // This should be an input file. Allow Picocli to process it as usual.
                return false
            }
            // This is neither our argument (these aren't passed here) nor javac's nor an input file.
            throw CommandLine.ParameterException(commandSpec.commandLine(), "ijavac: invalid flag: $arg", argSpec, arg)
        }
    }

    private fun tryConsumeToolsArgument(args: Stack<String>): Boolean {
        return tryConsumeToolArgument(args, javaCompiler) || tryConsumeToolArgument(args, fileManager)
    }

    private fun tryConsumeToolArgument(args: Stack<String>, tool: OptionChecker): Boolean {
        val arg = args.peek()
        val argCount = tool.isSupportedOption(arg)
        if (argCount >= 0) {
            for (i in 0..argCount) {
                otherJavacOptions.add(args.pop())
                if (i == 0) {
                    hasUnsupportedJavacOption =
                        hasUnsupportedJavacOption || isUnsupportedOption(otherJavacOptions.last())
                }
            }
            return true
        }
        return false
    }

    private fun isUnsupportedOption(javacOption: String): Boolean {
        return javacOption in UNSUPPORTED_OPTIONS || UNSUPPORTED_OPTION_PREFIXES.any { prefix ->
            javacOption.startsWith(
                prefix
            )
        }
    }
}

fun main(args: Array<String>) {
    exitProcess(Main().use { main ->
        CommandLine(main).setSeparator(" ").setUnmatchedOptionsArePositionalParams(true).execute(*args)
    })
}
