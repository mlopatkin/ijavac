val asResources: Configuration by configurations.creating

dependencies {
    api(project(":core"))

    asResources(project(":javac-plugin", "shadow"))

    testImplementation(testFixtures(project(":core")))
}

tasks.named<Copy>("processResources").configure {
    from(asResources) {// Add shadowed javac-plugin to this lib's resources
        rename {
            "javac-plugin.jar"
        }
    }
}
