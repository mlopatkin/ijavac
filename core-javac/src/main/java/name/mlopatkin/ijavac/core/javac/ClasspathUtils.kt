package name.mlopatkin.ijavac.core.javac

import java.io.File

private const val CLASSPATH_SEPARATOR = ':'

/**
 * Append a classpath entry (path to a jar or directory) to the (optional) classpath string (colon-separated list of
 * classpath entries). Returns the entry if there is nothing to append to. Returns original string unchanged if the
 * entry is `null` or empty string.
 */
internal fun String?.maybeAppendClasspathEntry(entry: String?): String? {
    if (entry.isNullOrEmpty()) return this
    if (this.isNullOrEmpty()) return entry
    return this + CLASSPATH_SEPARATOR + entry
}

/**
 * Splits a classpath string into a list of files, one file per entry. Empty list is returned if the classpath string is
 * `null`.
 */
internal fun String?.splitClasspathString(): List<File> {
    return this?.split(CLASSPATH_SEPARATOR)?.map { File(it) } ?: emptyList()
}
