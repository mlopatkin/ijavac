package name.mlopatkin.ijavac.core.javac

/**
 * Thrown when compilation fails.
 */
class CompileFailedException(msg: String) : Exception(msg) {
}
