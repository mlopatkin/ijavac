package name.mlopatkin.ijavac.core.javac

import name.mlopatkin.ijavac.core.CompiledClass
import name.mlopatkin.ijavac.core.ContextManager
import name.mlopatkin.ijavac.core.RelativePath
import name.mlopatkin.ijavac.core.SourceFile
import java.io.File
import java.net.URI
import java.util.Comparator
import javax.tools.JavaFileObject

/**
 * The compilation context to be used with JDK's Javac via compiler API.
 *
 * @param inputSources the inputs for the compilation
 */
internal class JavacCompilationContext(
    inputSources: Collection<SourceFile>,
    outputDir: File,
    precompiledClasses: Collection<CompiledClass>
) : ContextManager.Context<JavaFileObject> {
    private val outputDir = outputDir
    private val precompiledSourcesByBinaryName =
        precompiledClasses.associateBy(CompiledClass::binaryName, CompiledClass::source)

    // TODO(mlopatkin): we can replace URI lookup with JavaFileObjects identities
    private val inputSourcesByUri = inputSources.associateBy { src -> src.file }
    private val inputsByBinaryName = HashMap<String, JavaFileObject>()
    private val outputsByBinaryName = HashMap<String, JavaFileObject>()
    private val dependenciesByInputUri = HashMap<URI, Set<String>>()
    private val inputsWithStarImports = HashSet<URI>()

    override fun recordInput(binaryName: String, input: JavaFileObject) {
        inputsByBinaryName[binaryName] = input
    }

    override fun recordOutput(binaryName: String, output: JavaFileObject) {
        outputsByBinaryName[binaryName] = output
    }

    override fun recordDependencies(input: JavaFileObject, dependencies: Set<String>) {
        dependenciesByInputUri[input.getKeyUri()] = dependencies
    }

    override fun recordStarInput(input: JavaFileObject) {
        inputsWithStarImports.add(input.getKeyUri())
    }

    /**
     * Returns a set of [CompiledClass]es that were compiled within this context. It is expected that all inputs were
     * given inside the constructor.
     */
    fun getCompiledClasses(): Set<CompiledClass> {
        val result = HashSet<CompiledClass>()
        for (outputBinary in outputsByBinaryName) {
            val binaryName = outputBinary.key
            val outputObject = outputBinary.value
            val inputObject =
                inputsByBinaryName[binaryName] ?: throw IllegalStateException("Cannot find input for $binaryName")
            val inputObjectUri = inputObject.toUri().normalize()
            val inputSource = inputSourcesByUri[inputObjectUri]
                ?: throw IllegalStateException("Cannot find source for $binaryName at $inputObjectUri")
            result.add(CompiledClass(binaryName, inputSource, RelativePath(outputDir, File(outputObject.toUri()))))
        }
        return result
    }

    /**
     * Returns a map of the source-to-source dependencies.
     */
    fun getSourceDependencies(): Map<SourceFile, Set<SourceFile>> {
        val result = HashMap<SourceFile, Set<SourceFile>>()
        for (depsInfo in dependenciesByInputUri) {
            val inputSource =
                inputSourcesByUri[depsInfo.key] ?: throw IllegalStateException("Unlisted input source ${depsInfo.key}")
            val dependencies = depsInfo.value.asSequence()
                .mapNotNull { binary -> findSourceForBinaryName(binary) }
                .filterNot { src -> src == inputSource }
                .toSortedSet(Comparator.comparing { src -> src.file })

            result[inputSource] = dependencies
        }
        return result
    }

    private fun findSourceForBinaryName(binaryName: String): SourceFile? {
        val inputSource = inputsByBinaryName[binaryName].getKeyUri()?.let { uri -> inputSourcesByUri[uri] }
        val precompiledSource = precompiledSourcesByBinaryName[binaryName]
        if (inputSource != null && precompiledSource != null) {
            assert(inputSource != precompiledSource) {
                "Input source ${inputSource.file} is also precompiled: ${precompiledSource.file}"
            }
            // TODO(mlopatkin) This is actually a compilation error: duplicate class
            throw CompileFailedException(
                "Duplicate class $binaryName in precompiled source $precompiledSource and $inputSource")
        }
        return inputSource ?: precompiledSource
    }

    fun getSourcesWithStarImports(): Collection<SourceFile> {
        return inputsWithStarImports.map {
            inputSourcesByUri[it] ?: throw IllegalStateException("Unlisted input source $it")
        }
    }

    private fun JavaFileObject.getKeyUri(): URI {
        return toUri().normalize()
    }

    @JvmName("getKeyUriNullable")
    private fun JavaFileObject?.getKeyUri(): URI? {
        return this?.toUri()?.normalize()
    }
}
