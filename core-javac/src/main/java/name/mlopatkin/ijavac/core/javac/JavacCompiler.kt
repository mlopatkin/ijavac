package name.mlopatkin.ijavac.core.javac

import name.mlopatkin.ijavac.core.CompilationResult
import name.mlopatkin.ijavac.core.CompileFailure
import name.mlopatkin.ijavac.core.CompileSuccess
import name.mlopatkin.ijavac.core.CompiledClass
import name.mlopatkin.ijavac.core.Compiler
import name.mlopatkin.ijavac.core.ContextManager
import name.mlopatkin.ijavac.core.SourceFile
import name.mlopatkin.ijavac.core.makeLogger
import name.mlopatkin.ijavac.core.snapshot.Digest
import java.io.Closeable
import java.io.File
import java.io.IOException
import java.nio.file.Files
import javax.tools.JavaCompiler
import javax.tools.StandardJavaFileManager

private const val PLUGIN_JAR_RESOURCE_PATH = "/javac-plugin.jar"
private const val PLUGIN_NAME = "name.mlopatkin.ijavac.javacplugin.CompileInfoPlugin"

private val logger = makeLogger()

/**
 * An implementation of the [Compiler] that uses OpenJDK's javac via the Tool interface.
 */
class JavacCompiler(
    private val compilerImpl: JavaCompiler,
    private val fileManager: StandardJavaFileManager,
    private val classpathString: String? = null,
    private val javacOpts: List<String> = emptyList()
) : Compiler, Closeable {

    private val pluginJarDelegate = lazy {
        val jar = File.createTempFile("tmp", ".jar")
        javaClass.getResourceAsStream(PLUGIN_JAR_RESOURCE_PATH).use { input ->
            jar.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        return@lazy jar
    }
    private val pluginJar by pluginJarDelegate

    override fun compile(
        inputs: List<SourceFile>,
        outputDir: File,
        additionalClasspathDir: File?,
        precompiledClasses: Collection<CompiledClass>
    ): CompilationResult {
        val opts = ArrayList(javacOpts)
        opts += listOf(
            "-processorpath",
            pluginJar.path,
            "-Xplugin:$PLUGIN_NAME",
            "-d",
            outputDir.path,
        )
        val classpathStringToUse = classpathString.maybeAppendClasspathEntry(additionalClasspathDir?.path)

        if (classpathStringToUse != null) {
            opts += listOf("-cp", classpathStringToUse)
        }
        logger.debug("javac options {}", opts.joinToString(separator = " "))
        logger.debug("files to compile:\n{}", inputs.joinToString(separator = "\n"))

        val context = JavacCompilationContext(inputs, outputDir, precompiledClasses)
        val compilationTask = compilerImpl.getTask(
            /* out */ null,
            TrackingFileManager(context, fileManager),
            /* diagnosticListener */ null,
            opts,
            /* classes */ null,
            fileManager.getJavaFileObjectsFromFiles(inputs.map { src -> File(src.file) })
        )

        try {
            ContextManager.openContext(ContextManager.key(compilationTask), context).use {
                if (!compilationTask.call()) {
                    return CompileFailure
                }
            }
            return CompileSuccess(
                context.getCompiledClasses(),
                context.getSourceDependencies(),
                context.getSourcesWithStarImports()
            )
        } catch (e: CompileFailedException) {
            logger.error("Compilation failed", e)
            return CompileFailure
        }
    }

    override fun close() {
        if (pluginJarDelegate.isInitialized()) {
            if (!pluginJar.delete()) {
                throw IOException("Failed to delete ${pluginJar.path}")
            }
        }
    }

    // TODO(mlopatkin) the actual version of the javac and JDK also affects the build result. Find the way to plug it
    //  in.
    override val snapshotDigest: Digest<Compiler> = Digest.create {
        appendJavaObject(javacOpts)
        for (classpathEntry in classpathString.splitClasspathString()) {
            classpathEntry.walkTopDown().filter { it.isFile }.forEach {
                val timestamp = Files.getLastModifiedTime(it.toPath()).toInstant()
                appendTimestamp(timestamp)
            }
        }
    }
}
