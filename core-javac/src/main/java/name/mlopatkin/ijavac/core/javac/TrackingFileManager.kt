package name.mlopatkin.ijavac.core.javac

import name.mlopatkin.ijavac.core.ContextManager
import javax.tools.FileObject
import javax.tools.ForwardingJavaFileManager
import javax.tools.JavaFileManager
import javax.tools.JavaFileObject
import javax.tools.StandardJavaFileManager

/**
 * A [JavaFileManager] implementation that records all [JavaFileObject.Kind.CLASS] used for outputs in the provided
 * [ContextManager.Context].
 */
internal class TrackingFileManager(
    context: ContextManager.Context<JavaFileObject>,
    fileManager: StandardJavaFileManager
) : ForwardingJavaFileManager<StandardJavaFileManager>(fileManager) {

    private val context = context

    override fun getJavaFileForOutput(
        location: JavaFileManager.Location,
        className: String,
        kind: JavaFileObject.Kind,
        sibling: FileObject?
    ): JavaFileObject {
        val outputFile = super.getJavaFileForOutput(location, className, kind, sibling)
        if (kind == JavaFileObject.Kind.CLASS) {
            context.recordOutput(className, outputFile)
        }
        return outputFile
    }
}
