package name.mlopatkin.ijavac.core.javac

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class ClasspathUtilsKtTest {
    @Test
    internal fun `append path to classpath`() {
        assertThat("/path/to.jar:other.jar".maybeAppendClasspathEntry("some/dir"))
            .isEqualTo("/path/to.jar:other.jar:some/dir")
    }

    @Test
    internal fun `append null to classpath`() {
        assertThat("/path/to.jar:other.jar".maybeAppendClasspathEntry(null)).isEqualTo("/path/to.jar:other.jar")
    }

    @Test
    internal fun `append empty entry`() {
        assertThat("other.jar".maybeAppendClasspathEntry("")).isEqualTo("other.jar")
    }

    @Test
    internal fun `append entry to null`() {
        assertThat(null.maybeAppendClasspathEntry("other.jar")).isEqualTo("other.jar")
    }

    @Test
    internal fun `append empty to null`() {
        assertThat(null.maybeAppendClasspathEntry("")).isNull()
    }

    @Test
    internal fun `append null to null`() {
        assertThat(null.maybeAppendClasspathEntry(null)).isNull()
    }

    @Test
    internal fun `append entry to empty classpath`() {
        assertThat("".maybeAppendClasspathEntry("other.jar")).isEqualTo("other.jar")
    }

    @Test
    internal fun `append empty entry to empty classpath`() {
        assertThat("".maybeAppendClasspathEntry("")).isEqualTo("")
    }

    @Test
    internal fun `append null to empty classpath`() {
        assertThat("".maybeAppendClasspathEntry(null)).isEqualTo("")
    }
}
