package name.mlopatkin.ijavac.core.javac

import name.mlopatkin.ijavac.core.compiledClass
import name.mlopatkin.ijavac.core.source
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.File

internal class JavacCompilationContextTest {
    @Test
    internal fun `source dependencies are resolved even if dependencies not recompiled`() {
        val context = JavacCompilationContext(
            listOf(source("/tmp/Foo.java")),
            File("/tmp/bin"),
            listOf(compiledClass("Bar", "/tmp/Bar.java", "Bar.class"))
        )
        context.recordInput("Foo", sourceObject("/tmp/Foo.java"))
        context.recordDependencies(sourceObject("/tmp/Foo.java"), setOf("Bar"))
        context.recordOutput("Foo", classObject("/tmp/bin/Foo.class"))

        Assertions.assertThat(context.getSourceDependencies())
            .containsEntry(source("/tmp/Foo.java"), setOf(source("/tmp/Bar.java")))
    }

    @Test
    internal fun `exception is thrown when class can be resolved twice`() {
        val context = JavacCompilationContext(
            listOf(source("/tmp/Foo.java"), source("/tmp/Bar2.java")),
            File("/tmp/bin"),
            listOf(compiledClass("Bar", "/tmp/Bar.java", "Bar.class"))
        )
        context.recordInput("Foo", sourceObject("/tmp/Foo.java"))
        context.recordInput("Bar", sourceObject("/tmp/Bar2.java"))
        context.recordDependencies(sourceObject("/tmp/Foo.java"), setOf("Bar"))
        context.recordOutput("Foo", classObject("/tmp/bin/Foo.class"))
        context.recordOutput("Bar", classObject("/tmp/bin/Bar.class"))

        assertThrows<CompileFailedException> {
            context.getSourceDependencies()
        }
    }
}
