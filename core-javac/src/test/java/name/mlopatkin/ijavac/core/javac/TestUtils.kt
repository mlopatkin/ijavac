package name.mlopatkin.ijavac.core.javac

import java.io.File
import javax.tools.JavaFileObject
import javax.tools.SimpleJavaFileObject

private class TestJavaFileObject(path: String, kind: JavaFileObject.Kind) :
    SimpleJavaFileObject(File(path).toURI(), kind)

internal fun sourceObject(path: String): JavaFileObject {
    return TestJavaFileObject(path, JavaFileObject.Kind.SOURCE)
}

internal fun classObject(path: String): JavaFileObject {
    return TestJavaFileObject(path, JavaFileObject.Kind.CLASS)
}
