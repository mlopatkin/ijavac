package name.mlopatkin.ijavac.core

sealed class CompilationResult

data class CompileSuccess(
    val outputs: Collection<CompiledClass>,
    val depsSnapshot: Map<SourceFile, Set<SourceFile>>,
    val sourcesWithStarImports: Collection<SourceFile>
) : CompilationResult()

object CompileFailure : CompilationResult()

