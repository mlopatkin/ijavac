package name.mlopatkin.ijavac.core

/**
 * The output of the compilation - a compiled class file
 */
data class CompiledClass(
    val binaryName: String,
    val source: SourceFile,
    val outputPath: RelativePath
)
