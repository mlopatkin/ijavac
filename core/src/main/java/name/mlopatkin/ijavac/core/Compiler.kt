package name.mlopatkin.ijavac.core

import name.mlopatkin.ijavac.core.snapshot.Digest
import java.io.File

/**
 * Interface to interact with the platform's compiler (e. g. javac).
 */
interface Compiler {
    /**
     * Compiles the set of input sources and returns the result of the compilation.
     */
    fun compile(
        inputs: List<SourceFile>,
        outputDir: File,
        additionalClasspathDir: File? = null,
        precompiledClasses: Collection<CompiledClass> = emptyList()
    ): CompilationResult

    /**
     * The digest of the compiler configuration for the snapshotting.
     */
    val snapshotDigest: Digest<Compiler>
}
