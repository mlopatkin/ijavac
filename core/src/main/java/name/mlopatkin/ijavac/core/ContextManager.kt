package name.mlopatkin.ijavac.core

import name.mlopatkin.ijavac.core.ContextManager.Context
import name.mlopatkin.ijavac.core.ContextManager.findContext
import name.mlopatkin.ijavac.core.ContextManager.openContext
import java.io.Closeable

/**
 * The singleton that can be used to open a new compilation [Context] or obtain existing instance of the context. The
 * context is opened with a call to [openContext]. When opened, a context can be obtained with [findContext] if the
 * equivalent context key is passed into it. The context cannot be obtained anymore after it is closed.
 */
object ContextManager {
    // This class provides a simple way for the compiler plugins to talk back to us. The javac's Plugin API implies that
    // the plugin is loaded by the compiler internally with a child classloader. There is no way to access the plugin
    // object so we have to resort to singleton so the plugin can grab this object. ContextKey allows to have multiple
    // concurrent compilations and simplify testing by limiting the typical downsides of a singleton.

    private val contexts = HashMap<ContextKey<*, *>, Context<*>>()

    /**
     * The key to use when obtaining context. All keys with the equivalent tokens and same compilation unit class are
     * equal and can be used to lookup the context.
     *
     * @param T the type of the unique context's token (e.g. CompilationTask for Javac)
     * @param U the type of the compilation units used in the compiler's implementation
     *
     * @constructor Creates a new context key. It is easier to use [ContextManager.key] to obtain one though.
     */
    data class ContextKey<T, U>(private val scope: T, private val type: Class<U>)

    /**
     * Creates a new [ContextKey].
     */
    inline fun <T, reified U> key(scope: T): ContextKey<T, U> {
        return ContextKey(scope, U::class.java)
    }

    /**
     * The compilation context.
     *
     * @param U the type that is used to represent inputs and outputs in the particular implementation of the compiler
     */
    interface Context<U> {
        /**
         * Records that the class named [binaryName] was produces from [input].
         *
         * @param binaryName the binary name of the class that was produced
         * @param input the source of the class
         */
        fun recordInput(binaryName: String, input: U)

        /**
         * Records that the class named [binaryName] was stored in the [output].
         *
         * @param binaryName the binary name of the class that was produced
         * @param output the location of the compiled class
         */
        fun recordOutput(binaryName: String, output: U)

        /**
         * Records the collection of the binary names of dependencies of the given input. Transitive dependencies aren't
         * included.
         *
         * @param input the input for which dependencies are recorded
         * @param dependencies a collection of binary names of the dependencies
         */
        fun recordDependencies(input: U, dependencies: Set<String>)

        /**
         * Records that the given input contains a non-static star import.
         *
         * @param the input that has a star import
         */
        fun recordStarInput(input: U)
    }

    /**
     * Opens a context and returns a handle that can be used to close it. The [context] will be returned from
     * [findContext] if the matching key is used until it is closed. The function throws an exception if there is
     * an open context for the given key already.
     *
     * @param contextKey the key to associate the context with
     * @param context the context to open
     * @return the handle to close the context. It is an error to close context twice though.
     * @throws IllegalArgumentException if there is an open context for the given key
     */
    fun <T, U> openContext(contextKey: ContextKey<T, U>, context: Context<U>): Closeable {
        if (contexts.containsKey(contextKey)) {
            throw IllegalArgumentException("Context is already opened for the key $contextKey")
        }
        contexts[contextKey] = context
        return Closeable { closeContext(contextKey) }
    }

    private fun <T, U> closeContext(contextKey: ContextKey<T, U>) {
        if (contexts.remove(contextKey) == null) {
            throw IllegalArgumentException("There is no open context for $contextKey")
        }
    }

    /**
     * Finds an opened context for the given key.
     *
     * @param contextKey the key to lookup context with
     * @throws IllegalArgumentException if there is no open context for the given key
     */
    @Suppress("UNCHECKED_CAST")
    fun <T, U> findContext(contextKey: ContextKey<T, U>): Context<U> {
        val scope = contexts[contextKey] ?: throw IllegalArgumentException("There is no open context for $contextKey")
        return scope as Context<U>
    }
}
