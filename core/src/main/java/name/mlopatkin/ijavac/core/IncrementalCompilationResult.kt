package name.mlopatkin.ijavac.core

enum class NonIncrementalReason(val description: String) {
    DISABLED("incremental compilation is disabled"),
    FALLBACK("incremental compilation failed, non-incremental was used as a fallback"),
    INCOMPATIBLE_CHANGES("incompatible changes detected"),
}

sealed class IncrementalCompilationResult {
    abstract val success: Boolean

    class Success(val recompiledInputs: Collection<SourceFile>) : IncrementalCompilationResult() {
        override val success = true
    }

    class NonIncrementalSuccess(val reason: NonIncrementalReason, val additionalDescription: String? = null) :
        IncrementalCompilationResult() {
        override val success = true
    }

    class Failure(val reason: CompileFailure) : IncrementalCompilationResult() {
        override val success = false
    }
}
