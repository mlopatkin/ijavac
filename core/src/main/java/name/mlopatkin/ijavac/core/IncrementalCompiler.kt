package name.mlopatkin.ijavac.core

import name.mlopatkin.ijavac.core.snapshot.ChangedSources
import name.mlopatkin.ijavac.core.snapshot.FullRecompilationRequired
import name.mlopatkin.ijavac.core.snapshot.NoChanges
import name.mlopatkin.ijavac.core.snapshot.PostCompilationSnapshot
import name.mlopatkin.ijavac.core.snapshot.PreCompilationSnapshot
import name.mlopatkin.ijavac.core.snapshot.SnapshotFile
import name.mlopatkin.ijavac.core.snapshot.computeDifference
import name.mlopatkin.ijavac.core.snapshot.takeSnapshotOfInputs
import java.io.File
import java.io.IOException

private val logger = makeLogger()

/**
 * This class serves as the main entry point. It actually performs incremental compilations.
 *
 * @constructor Creates the instance of the IncrementalCompiler that uses provided [Compiler] to do actual compilations.
 * The compilation won't be incremental if the snapshot is not provided.
 *
 * @param compiler the compiler to do the compilation
 * @param snapshotFile the snapshot file of the compilation state or `null` to disable incrementalism
 */
class IncrementalCompiler(
    private val outputDir: File,
    private val compiler: Compiler,
    private val snapshotFile: SnapshotFile?
) {
    /**
     * Compiles the given files.
     *
     * @return the result of the compilation
     */
    fun compile(inputs: List<SourceFile>): IncrementalCompilationResult {
        if (snapshotFile == null) {
            // run in non-incremental mode
            return when (val result = compiler.compile(inputs, outputDir)) {
                is CompileSuccess -> IncrementalCompilationResult.NonIncrementalSuccess(NonIncrementalReason.DISABLED)
                is CompileFailure -> IncrementalCompilationResult.Failure(result)
            }
        }

        val prevSnapshot = snapshotFile.loadPreviousSnapshot()
        val curSnapshot = takeSnapshotOfInputs(inputs, compiler.snapshotDigest)
        return when (val diff = computeDifference(prevSnapshot, curSnapshot)) {
            is FullRecompilationRequired -> performFullRecompilation(diff, inputs, prevSnapshot, curSnapshot)
            is ChangedSources -> performPartialCompilation(diff, inputs, prevSnapshot, curSnapshot)
            is NoChanges -> onlyRecompileMissing(inputs, prevSnapshot)
        }
    }

    private fun performFullRecompilation(
        diff: FullRecompilationRequired,
        inputs: List<SourceFile>,
        prevSnapshot: PostCompilationSnapshot,
        curSnapshot: PreCompilationSnapshot,
        disabledReason: NonIncrementalReason = NonIncrementalReason.INCOMPATIBLE_CHANGES
    ): IncrementalCompilationResult {
        logger.info("Full recompilation required: {}", diff.reason)
        cleanStaleOutputs(prevSnapshot.getAllOutputs().map { it.outputPath })
        val result = compiler.compile(inputs, outputDir)
        if (result is CompileSuccess) {
            snapshotFile?.saveCurrentSnapshot(
                curSnapshot.applyCompilationInformation(
                    outputs = result.outputs,
                    deps = result.depsSnapshot,
                    sourcesWithStarImports = result.sourcesWithStarImports
                )
            )
        }
        return when (result) {
            is CompileSuccess -> IncrementalCompilationResult.NonIncrementalSuccess(
                disabledReason,
                additionalDescription = diff.reason
            )
            is CompileFailure -> IncrementalCompilationResult.Failure(result)
        }
    }

    private fun onlyRecompileMissing(
        inputs: List<SourceFile>,
        prevSnapshot: PostCompilationSnapshot
    ): IncrementalCompilationResult {
        logger.info("No changes")
        val dirtyInputs = getSourcesWithMissingOutputs(prevSnapshot, inputs)
        if (dirtyInputs.isEmpty()) {
            return IncrementalCompilationResult.Success(recompiledInputs = dirtyInputs)
        }
        return when (val result = compiler.compile(dirtyInputs, outputDir, additionalClasspathDir = outputDir)) {
            is CompileSuccess -> {
                IncrementalCompilationResult.Success(recompiledInputs = dirtyInputs)
            }
            is CompileFailure -> {
                snapshotFile?.removeSnapshot()
                IncrementalCompilationResult.Failure(result)
            }
        }
    }

    private fun performPartialCompilation(
        diff: ChangedSources,
        inputs: List<SourceFile>,
        prevSnapshot: PostCompilationSnapshot,
        curSnapshot: PreCompilationSnapshot
    ): IncrementalCompilationResult {
        logger.info("Partial recompilation needed")
        // Outputs of the removed sources have to be removed
        val filesToCleanOutputs = HashSet<SourceFile>(diff.removedSources)

        // Compute the list of sources that needs to be (re)compiled
        val dirtyInputs = HashSet<SourceFile>()
        // All changed sources and their dependents must be recompiled.  Their previous outputs are known have to be
        // removed.
        dirtyInputs.addAll(diff.changedSources)
        prevSnapshot.getDependentsTo(dirtyInputs, dirtyInputs)
        // Removed sources may also affect some sources (but new sources may compensate for it, e. g. if file rename
        // happened). Add their dependencies to the recompilation round.
        prevSnapshot.getDependentsTo(dirtyInputs, diff.removedSources)
        // Each of this sources misses at least one output but others outputs may be intact so cleanup is needed as well
        getSourcesWithMissingOutputsTo(dirtyInputs, prevSnapshot, dirtyInputs)
        // Files with star imports should also be recompiled
        prevSnapshot.getSourcesWithStarImportsTo(dirtyInputs)
        filesToCleanOutputs.addAll(dirtyInputs)
        cleanStaleOutputs(prevSnapshot, filesToCleanOutputs)
        // Newly added sources have no outputs or any information in the previous snapshot so add them after the cleanup
        dirtyInputs.addAll(diff.addedSources)

        ScopedTempDir().use { intermediateDirHolder ->
            val intermediateDir = intermediateDirHolder.dir

            val result = if (dirtyInputs.isNotEmpty()) {
                compiler.compile(
                    inputs.filter { it in dirtyInputs },
                    outputDir = intermediateDir,
                    additionalClasspathDir = outputDir,
                    precompiledClasses = prevSnapshot.getAllOutputs().filter { it.source !in dirtyInputs }
                )
            } else {
                CompileSuccess(outputs = emptyList(), depsSnapshot = emptyMap(), sourcesWithStarImports = emptyList())
            }

            if (result is CompileSuccess && checkOutputs(result.outputs, filesToCleanOutputs, prevSnapshot)) {
                result.outputs.forEach { it.copyFrom(intermediateDir) }

                val updatedSnapshot =
                    curSnapshot.applyPartialCompilationInformation(
                        partialOutputs = result.outputs,
                        partialDeps = result.depsSnapshot,
                        partialSourcesWithStarImports = result.sourcesWithStarImports,
                        previousCompilationSnapshot = prevSnapshot
                    )

                snapshotFile?.saveCurrentSnapshot(updatedSnapshot)

                return IncrementalCompilationResult.Success(dirtyInputs)
            } else {
                return performFullRecompilation(
                    FullRecompilationRequired("incremental compilation failed"),
                    inputs,
                    prevSnapshot,
                    curSnapshot,
                    disabledReason = NonIncrementalReason.FALLBACK
                )
            }
        }
    }

    private fun cleanStaleOutputs(staleOutputs: Collection<RelativePath>) {
        staleOutputs.forEach { path ->
            val staleOutput = path.resolveIn(outputDir)
            if (staleOutput.exists()) {
                logger.debug("Removing stale output file: {}", staleOutput)
                if (!staleOutput.delete()) {
                    throw IOException("Failed to delete $staleOutput")
                }
            }
        }
    }

    private fun cleanStaleOutputs(snapshot: PostCompilationSnapshot, removedFiles: Collection<SourceFile>) {
        removedFiles.forEach { removedFile ->
            cleanStaleOutputs(
                snapshot.getOutputs(removedFile).map { it.outputPath })
        }
    }

    private fun getSourcesWithMissingOutputs(
        prevSnapshot: PostCompilationSnapshot,
        sources: List<SourceFile>
    ): List<SourceFile> {
        return getSourcesWithMissingOutputsTo(arrayListOf(), prevSnapshot, sources)
    }

    private fun <T : MutableCollection<SourceFile>> getSourcesWithMissingOutputsTo(
        destination: T,
        prevSnapshot: PostCompilationSnapshot,
        sources: Collection<SourceFile>
    ): T {
        return sources.filterTo(destination) { source -> prevSnapshot.hasMissingOutputs(source) }
    }

    private fun PostCompilationSnapshot.hasMissingOutputs(source: SourceFile): Boolean {
        return getOutputs(source).any { outputClass -> !outputClass.outputPath.resolveIn(outputDir).exists() }
    }

    private fun CompiledClass.copyFrom(intermediateDir: File) {
        val source = outputPath.resolveIn(intermediateDir)
        val destination = outputPath.resolveIn(outputDir)
        val destDir = destination.parentFile
        if (!destDir.exists() && !destDir.mkdirs()) {
            throw IOException("Failed to create dir at $destDir")
        }
        if (destination.exists() && !destination.delete()) {
            throw IOException("Destination file $destination already exists and cannot be deleted")
        }
        if (!source.renameTo(destination)) {
            throw IOException("Failed to move $source into $destination")
        }
    }

    private fun checkOutputs(
        outputs: Collection<CompiledClass>,
        cleanedFiles: Collection<SourceFile>,
        prevSnapshot: PostCompilationSnapshot
    ): Boolean {
        val actualOutputs = outputs.associateBy { it.binaryName }
        prevSnapshot.getAllOutputs().forEach { prevOutput ->
            val newOutputForBinary = actualOutputs[prevOutput.binaryName]
            if (newOutputForBinary != null
                && newOutputForBinary.source != prevOutput.source && prevOutput.source !in cleanedFiles
            ) {
                logger.error(
                    "Duplicate class {} detected in {}",
                    newOutputForBinary.binaryName,
                    newOutputForBinary.source
                )
                return false

            }
        }
        return true
    }
}
