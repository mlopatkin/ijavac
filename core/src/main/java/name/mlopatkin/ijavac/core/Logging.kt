package name.mlopatkin.ijavac.core

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.invoke.MethodHandles

/**
 * Utility function  to create a logger. Use it to initialize a top-level `private val logger` reference.
 */
@Suppress("NOTHING_TO_INLINE")
inline fun makeLogger(): Logger {
    return LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
}
