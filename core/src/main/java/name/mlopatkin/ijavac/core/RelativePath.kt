package name.mlopatkin.ijavac.core

import java.io.File

/**
 * The relative path. It can only be resolved in some base directory.
 */
data class RelativePath(val value: String) {
    constructor(baseDir: File, file: File) : this(file.absoluteFile.toRelativeString(baseDir.absoluteFile))

    fun resolveIn(baseDir: File): File {
        return File(baseDir, value)
    }
}
