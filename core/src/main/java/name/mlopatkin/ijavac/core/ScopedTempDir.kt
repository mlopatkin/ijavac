package name.mlopatkin.ijavac.core

import java.io.Closeable
import java.io.File
import java.io.IOException
import java.nio.file.Files

class ScopedTempDir(prefix: String = "tmpCompile") : Closeable {
    val dir: File = Files.createTempDirectory(prefix).toFile()

    override fun close() {
        if (!dir.deleteRecursively()) {
            throw IOException("Failed to delete $dir")
        }
    }
}
