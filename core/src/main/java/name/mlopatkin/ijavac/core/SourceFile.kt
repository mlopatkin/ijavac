package name.mlopatkin.ijavac.core

import java.io.File
import java.net.URI

/**
 * The source file to be compiled.
 */
class SourceFile(file: File) {
    val file: URI = file.toURI().normalize()

    override fun toString(): String {
        return "SourceFile(file=${file.path})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return (file == (other as SourceFile).file)
    }

    override fun hashCode(): Int {
        return file.hashCode()
    }
}
