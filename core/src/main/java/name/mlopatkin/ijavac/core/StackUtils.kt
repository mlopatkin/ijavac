package name.mlopatkin.ijavac.core

// Utility functions to make stack out of an ArrayList.
/**
 * Removes the last element of the ArrayList and returns the removed element.
 *
 * @return the value removed from the end of the list
 * @throws NoSuchElementException if the list is empty
 */
internal fun <T> ArrayList<T>.popStack(): T {
    val result = last()
    removeAt(size - 1)
    return result
}

/**
 * Adds a value to the end of the ArrayList.
 *
 * @param value a value to add
 */
internal fun <T> ArrayList<T>.pushStack(value: T) {
    add(value)
}
