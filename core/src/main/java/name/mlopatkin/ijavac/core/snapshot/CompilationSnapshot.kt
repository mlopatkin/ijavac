package name.mlopatkin.ijavac.core.snapshot

import name.mlopatkin.ijavac.core.CompiledClass
import name.mlopatkin.ijavac.core.Compiler
import name.mlopatkin.ijavac.core.RelativePath
import name.mlopatkin.ijavac.core.SourceFile
import name.mlopatkin.ijavac.core.makeLogger
import name.mlopatkin.ijavac.core.popStack
import name.mlopatkin.ijavac.core.pushStack
import name.mlopatkin.ijavac.core.snapshot.Digest.Companion.nullDigest
import name.mlopatkin.ijavac.core.snapshot.Digest.Companion.readDigest
import name.mlopatkin.ijavac.core.snapshot.Digest.Companion.writeDigest
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Base interface for the compilation snapshot. Implementation of this interface are expected to be effectively
 * immutable. This interface and its sub-interfaces are not expected to be implemented or extended outside of the file
 * it is declared in.
 */
internal interface CompilationSnapshotBase {
    // The interface exists to prevent PostCompilationSnapshot from having apply functions.
    /** The digest of the compiler used for the snapshotted compilation. */
    val compilerDigest: Digest<Compiler>

    /** The inputs to the snapshotted compilation. */
    val inputs: Set<SourceFile>

    /**
     * Returns the digest of the given input file. The input file must be in [inputs].
     * @param input the file to get digest of
     * @return the digest of the given input source file
     * @throws IllegalArgumentException if the `input` is not in [inputs]
     */
    fun getDigest(input: SourceFile): Digest<SourceFile>
}

/**
 * Snapshot taken before the actual compilation. It doesn't have information about compilation outputs or inter-source
 * dependencies.
 */
internal interface PreCompilationSnapshot : CompilationSnapshotBase {
    /**
     * Converts this snapshot to a post-compilation snapshot by applying updates from the results of partial compilation
     * and the snapshot of the previous compilation. The latter is used to retrieve information about sources that are
     * still listed as inputs but not recompiled this time. This method doesn't modify the object. It is assumed that
     * the partial compilation affected only new and changed input files.
     *
     * @param partialOutputs the outputs of the partial compilation
     * @param partialDeps the dependency information from the partial compilation
     * @param partialSourcesWithStarImports the list of inputs that have star imports in them
     * @param previousCompilationSnapshot the snapshot of the previous compilation
     * @return the new PostCompilationSnapshot
     */
    fun applyPartialCompilationInformation(
        partialOutputs: Collection<CompiledClass>,
        partialDeps: Map<SourceFile, Collection<SourceFile>>,
        partialSourcesWithStarImports: Collection<SourceFile>,
        previousCompilationSnapshot: PostCompilationSnapshot
    ): PostCompilationSnapshot

    /**
     * Converts this snapshot to a post-compilation snapshot by applying updates from the results of the full
     * compilation. This method doesn't modify the object.
     *
     * @param outputs the outputs of the compilation
     * @param deps the dependency information from the compilation
     * @param sourcesWithStarImports the list of inputs that have star imports in them
     * @return the new PostCompilationSnapshot
     */
    fun applyCompilationInformation(
        outputs: Collection<CompiledClass>,
        deps: Map<SourceFile, Collection<SourceFile>>,
        sourcesWithStarImports: Collection<SourceFile>
    ): PostCompilationSnapshot
}

/**
 * Snapshot taken after the compilation. It contains additional information that is collected during the compilation
 * process. Note that snapshot contains information about
 */
internal interface PostCompilationSnapshot : CompilationSnapshotBase {
    /**
     * Returns the outputs produced during the compilation of the given input file. The input file must be in [inputs].
     * @param input the file to get outputs for
     * @return the outputs of the compilation, may be empty
     * @throws IllegalArgumentException if the `input` is not in [inputs]
     */
    fun getOutputs(input: SourceFile): Collection<CompiledClass>

    /**
     * Returns all outputs produced during compilation of the inputs.
     */
    fun getAllOutputs(): Collection<CompiledClass>

    /**
     * Serializes this snapshot to stream. It is possible to read the snapshot with [loadSnapshot] later.
     *
     * @param output the stream to save snapshot to
     * @throws java.io.IOException if something wrong happens during save
     */
    fun saveTo(output: OutputStream)

    /**
     * Appends all source files that directly or through their dependencies depend on any of the inputs to the
     * destination collection. Note that dependents do not include `inputs`. It is fine to have the same collection as
     * both `destination` and `inputs`.
     *
     * @param destination the collection to save dependents to
     * @param inputs the inputs to compute dependents
     * @return the `destination`
     * @throws IllegalArgumentException if any of the `inputs` is not in [inputs]
     */
    fun <C : MutableCollection<SourceFile>> getDependentsTo(destination: C, inputs: Collection<SourceFile>): C

    /**
     * Appends all source files with star imports to the destination collection. Such source files have to be recompiled
     * every time something changes because almost any change can introduce new or remove old classes in the imported
     * package and this can break compilation without explicit dependency on the changed source.
     */
    fun <C : MutableCollection<SourceFile>> getSourcesWithStarImportsTo(destination: C): C
}

/**
 * Takes a snaphot of the inputs. Note that source file digests saved in snapshot won't be updated afterwards.
 *
 * @param inputs the source files to take snapshot of
 * @param compilerDigest the digest of the compiler that is going to be used
 * @param fileDigestFunc the function to compute digest of file. Optional, file last modification time is used if
 * omitted
 * @return the PreCompilationSnapshot of the files and the compiler
 *
 * @throws java.io.IOException if `fileDigestFunc` throws during computing digest
 */
internal fun takeSnapshotOfInputs(
    inputs: Collection<SourceFile>,
    compilerDigest: Digest<Compiler>,
    fileDigestFunc: (SourceFile) -> Digest<SourceFile> = SourceFile::mTimeBasedDigest
): PreCompilationSnapshot {
    return CompilationSnapshot(inputs, compilerDigest, fileDigestFunc)
}

/**
 * Loads snapshot saved with [PostCompilationSnapshot.saveTo].
 *
 * @return restored PostCompilationSnapshot
 * @throws java.io.IOException if reading fails
 */
internal fun loadSnapshot(input: InputStream): PostCompilationSnapshot {
    return CompilationSnapshot.loadFromFile(input)
}

/**
 * @return an empty PostCompilationSnapshot that contains no inputs and no compiler info.
 */
internal fun emptySnapshot(): PostCompilationSnapshot {
    return CompilationSnapshot(compilerDigest = nullDigest())
}

private val logger = makeLogger()

/**
 * The snapshot of a single compilation invocation: what compiler was used, what files were compiled, etc.
 */
private class CompilationSnapshot private constructor(
    override val compilerDigest: Digest<Compiler>,
    private val inputSnapshots: List<SourceFileSnapshot>
) : PreCompilationSnapshot, PostCompilationSnapshot {
    private val snapshotsByInput: Map<SourceFile, SourceFileSnapshot> =
        inputSnapshots.associateBy { it.source }

    override val inputs: Set<SourceFile>
        get() = snapshotsByInput.keys

    constructor(
        inputs: Collection<SourceFile> = emptyList(),
        compilerDigest: Digest<Compiler> = nullDigest(),
        fileDigest: (SourceFile) -> Digest<SourceFile> = SourceFile::mTimeBasedDigest
    ) : this(
        compilerDigest,
        inputs.mapIndexed { i, source -> SourceFileSnapshot(i, source, fileDigest(source)) })

    override fun getDigest(input: SourceFile): Digest<SourceFile> {
        return snapshotsByInput[input]?.digest ?: throw IllegalArgumentException("Unknown file $input")
    }

    override fun getOutputs(input: SourceFile): Collection<CompiledClass> {
        return snapshotsByInput[input]?.outputs ?: throw IllegalArgumentException("Unknown file $input")
    }

    override fun getAllOutputs(): Collection<CompiledClass> {
        return inputSnapshots.flatMap { it.outputs }.toList()
    }

    override fun saveTo(output: OutputStream) {
        val dos = DataOutputStream(output)
        dos.writeDigest(compilerDigest)
        dos.writeList(inputSnapshots, dos::writeSnapshotEntry)
        dos.writeList(inputSnapshots, dos::writeDepsEntry)
    }

    override fun applyCompilationInformation(
        outputs: Collection<CompiledClass>,
        deps: Map<SourceFile, Collection<SourceFile>>,
        sourcesWithStarImports: Collection<SourceFile>
    ): PostCompilationSnapshot {
        return copy().apply {
            applyOutputs(outputs, sourcesWithStarImports)
            applyDependencies(deps)
        }
    }

    override fun applyPartialCompilationInformation(
        partialOutputs: Collection<CompiledClass>,
        partialDeps: Map<SourceFile, Collection<SourceFile>>,
        partialSourcesWithStarImports: Collection<SourceFile>,
        previousCompilationSnapshot: PostCompilationSnapshot
    ): PostCompilationSnapshot {
        return copy().apply {
            // TODO(mlopatkin) This is quite a shortcut but I don't expect non-CompilationSnapshot implementations
            applyFromPreviousSnapshot((previousCompilationSnapshot as CompilationSnapshot).inputSnapshots)
            applyOutputs(partialOutputs, partialSourcesWithStarImports)
            applyDependencies(partialDeps)
        }
    }

    private fun copy(): CompilationSnapshot {
        return CompilationSnapshot(compilerDigest, inputSnapshots.map { it.copyNoRefs() })
    }

    private fun applyOutputs(outputs: Collection<CompiledClass>, sourcesWithStarImports: Collection<SourceFile>) {
        val outputMap = outputs.groupBy(CompiledClass::source) { it }
        outputMap.forEach { (source, outputs) ->
            val snapshot = getSnapshotFor(source)
            snapshot.outputs = outputs
            snapshot.hasStarImport = source in sourcesWithStarImports
        }
    }

    private fun applyDependencies(deps: Map<SourceFile, Collection<SourceFile>>) {
        deps.forEach { (source, deps) ->
            getSnapshotFor(source).dependencies = getSnapshotsFor(deps)
        }
    }

    private fun applyFromPreviousSnapshot(prevSnapshots: Collection<SourceFileSnapshot>) {
        prevSnapshots.forEach { prevSnapshot ->
            val mySnapshot = snapshotsByInput[prevSnapshot.source]
            // mySnapshot may be null if the input is no longer compiled
            if (mySnapshot != null) {
                mySnapshot.dependencies = getSnapshotsFor(prevSnapshot.dependencies.map { d -> d.source })
                mySnapshot.outputs = prevSnapshot.outputs
                mySnapshot.hasStarImport = prevSnapshot.hasStarImport
            }
        }
    }

    override fun <C : MutableCollection<SourceFile>> getDependentsTo(
        destination: C,
        inputs: Collection<SourceFile>
    ): C {
        val visitedFiles = HashSet<SourceFileSnapshot>(getSnapshotsFor(inputs))
        val stack = ArrayList<SourceFileSnapshot>(visitedFiles)
        while (stack.isNotEmpty()) {
            val file = stack.popStack()
            file.dependents.forEach { dependentFile ->
                if (dependentFile !in visitedFiles) {
                    logger.debug("Dependents: {} -> {}", file.source, dependentFile.source)
                    stack.pushStack(dependentFile)
                    visitedFiles.add(dependentFile)
                    destination.add(dependentFile.source)
                }
            }
        }
        return destination
    }

    override fun <C : MutableCollection<SourceFile>> getSourcesWithStarImportsTo(destination: C): C {
        inputSnapshots.asSequence().filter { it.hasStarImport }.mapTo(destination) { it.source }
        return destination
    }

    private fun getSnapshotFor(inputSource: SourceFile): SourceFileSnapshot {
        return snapshotsByInput.getValue(inputSource)
    }

    private fun getSnapshotsFor(inputSources: Iterable<SourceFile>): Collection<SourceFileSnapshot> {
        return inputSources.map(this::getSnapshotFor)
    }

    companion object {
        fun loadFromFile(input: InputStream): CompilationSnapshot {
            val dis = DataInputStream(input)
            val compilerDigest = dis.readDigest<Compiler>()
            val inputSnapshots = dis.readListIndexed(dis::readSnapshotEntry)
            dis.readItemsIndexed { index ->
                dis.readDepsEntryTo(index, inputSnapshots)
            }
            return CompilationSnapshot(compilerDigest, inputSnapshots)
        }
    }
}

private val SourceFile.mTimeBasedDigest: Digest<SourceFile>
    get() = Digest.createForTimestamp(Files.getLastModifiedTime(Paths.get(file)).toInstant())

// This class is mutable because it contains references to other instances and these references may form cycles.
private class SourceFileSnapshot(
    val index: Int,
    val source: SourceFile,
    val digest: Digest<SourceFile>,
    var hasStarImport: Boolean = false,
    var outputs: Collection<CompiledClass> = emptyList()
) {
    private val _dependents = ArrayList<SourceFileSnapshot>()
    val dependents: List<SourceFileSnapshot> = _dependents

    private var _dependencies: Collection<SourceFileSnapshot> = emptyList()
    var dependencies: Collection<SourceFileSnapshot>
        get() = _dependencies
        set(dependencies) {
            _dependencies = dependencies
            _dependencies.forEach { dependency ->
                dependency._dependents.add(this)
            }
        }

    /**
     * Performs a copy of the snapshot. Only snapshots without outputs and dependency info can be copied.
     *
     * @return the copy of this instance
     */
    fun copyNoRefs(): SourceFileSnapshot {
        if (outputs.isNotEmpty() || _dependents.isNotEmpty() || _dependencies.isNotEmpty()) {
            throw IllegalStateException("Dependency info or outputs is not empty, cannot copy")
        }
        return SourceFileSnapshot(index, source, digest, hasStarImport)
    }
}

private fun DataOutputStream.writeDepsEntry(snapshot: SourceFileSnapshot) {
    writeList(snapshot.dependencies) { dep -> writeInt(dep.index) }
}

private fun DataInputStream.readDepsEntryTo(index: Int, snapshots: List<SourceFileSnapshot>) {
    val snapshotToRead = snapshots[index]
    snapshotToRead.dependencies = readList {
        snapshots[readInt()]
    }
}

private fun DataOutputStream.writeSnapshotEntry(snapshot: SourceFileSnapshot) {
    writeSourceFile(snapshot.source)
    writeDigest(snapshot.digest)
    writeBoolean(snapshot.hasStarImport)
    writeList(snapshot.outputs) { output -> writeCompiledClass(output) }
}

private fun DataInputStream.readSnapshotEntry(index: Int): SourceFileSnapshot {
    val source = readSourceFile()
    val digest = readDigest<SourceFile>()
    val hasStarImport = readBoolean()
    val outputs = readList { readCompiledClass(source) }
    return SourceFileSnapshot(index, source, digest, hasStarImport, outputs)
}

// TODO(mlopatkin) we probably should not use absolute paths here, it makes compilation dir non-relocatable
private fun DataOutputStream.writeSourceFile(f: SourceFile) {
    writeUTF(f.file.path)
}

private fun DataInputStream.readSourceFile(): SourceFile {
    return SourceFile(File(readUTF()))
}

private fun DataOutputStream.writeCompiledClass(compiledClass: CompiledClass) {
    writeUTF(compiledClass.binaryName)
    writeUTF(compiledClass.outputPath.value)
}

private fun DataInputStream.readCompiledClass(source: SourceFile): CompiledClass {
    val binaryName = readUTF()
    val outputPath = RelativePath(readUTF())

    return CompiledClass(binaryName, source, outputPath)
}

private fun <E> DataOutputStream.writeList(l: Collection<E>, itemWriter: (E) -> Unit) {
    writeInt(l.size)
    l.forEach(itemWriter)
}

private fun <E> DataInputStream.readList(itemReader: () -> E): List<E> {
    return readListIndexed { itemReader() }
}

private fun DataInputStream.readItemsIndexed(itemReader: (Int) -> Unit) {
    val size = readInt()
    for (i in 0 until size) {
        itemReader(i)
    }
}

private fun <E> DataInputStream.readListIndexed(itemReader: (Int) -> E): List<E> {
    val size = readInt()
    val items = ArrayList<E>(size)
    for (i in 0 until size) {
        items.add(itemReader(i))
    }
    return items
}
