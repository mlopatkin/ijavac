package name.mlopatkin.ijavac.core.snapshot

import com.google.common.hash.HashCode
import com.google.common.hash.Hashing
import java.io.InputStream
import java.io.OutputStream
import java.time.Instant

/**
 * The "modification witness" of some object. Typically these are used to compare the state of the object between two
 * snapshots: if [Digest]s are different then the object was changed.
 *
 * @param T the type of the object for which a hashcode was computed
 */
@Suppress("UnstableApiUsage")
class Digest<T> private constructor(private val bits: HashCode) {
    // TODO(mlopatkin) This digest implementation is a stub until I figure out a proper way to do serialization.
    /**
     * Checks if this digest is equal to the other. It isn't wise to use [equals] with Digest.
     */
    fun isEqualTo(other: Digest<T>?): Boolean {
        return this == other
    }

    override fun equals(other: Any?): Boolean {
        return this === other || bits == (other as? Digest<*>)?.bits
    }

    override fun hashCode(): Int {
        return bits.hashCode()
    }

    override fun toString(): String {
        return bits.toString()
    }


    class Builder<T> {
        private val hash = hasher.newHasher();
        fun appendJavaObject(obj: Any) {
            hash.putInt(obj.hashCode())
        }

        fun appendTimestamp(timestamp: Instant) {
            hash.putLong(timestamp.toEpochMilli())
        }

        fun build(): Digest<T> {
            return Digest(hash.hash())
        }
    }

    companion object {
        private val hasher = Hashing.sha256()

        /**
         * Creates an empty digest.
         */
        fun <T> nullDigest(): Digest<T> {
            return Digest(hasher.hashInt(0))
        }

        fun <T> create(block: Builder<T>.() -> Unit): Digest<T> {
            return Builder<T>().apply(block).build()
        }

        /**
         * Creates a digest based on a [hashCode] of the give Java object.
         */
        fun <T> createForJavaObject(obj: Any): Digest<T> {
            return createFromLong(obj.hashCode().toLong())
        }

        fun <T> createForTimestamp(timestamp: Instant): Digest<T> {
            return createFromLong(timestamp.toEpochMilli())
        }

        fun <T> createForTesting(v: Long): Digest<T> {
            return createFromLong(v)
        }

        private fun <T> createFromLong(v: Long): Digest<T> {
            return Digest(hasher.hashLong(v))
        }

        internal fun <T> InputStream.readDigest(): Digest<T> {
            val bytes = ByteArray(hasher.bits() / Byte.SIZE_BITS)
            read(bytes)
            return Digest(HashCode.fromBytes(bytes))
        }

        internal fun <T> OutputStream.writeDigest(digest: Digest<T>) {
            write(digest.bits.asBytes())
        }
    }
}
