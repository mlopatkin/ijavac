package name.mlopatkin.ijavac.core.snapshot

import name.mlopatkin.ijavac.core.SourceFile

/**
 * The difference between two compilation snapshots.
 */
internal sealed class SnapshotDiff

/**
 * The difference between snapshots is too big and the full recompilation is needed.
 *
 * @param reason the human-readable reason for the recompilation
 */
internal data class FullRecompilationRequired(val reason: String) : SnapshotDiff()

internal object NoChanges : SnapshotDiff()

internal data class ChangedSources(
    val unchangedSources: Collection<SourceFile>,
    val changedSources: Collection<SourceFile>,
    val addedSources: Collection<SourceFile>,
    val removedSources: Collection<SourceFile>
) : SnapshotDiff()

internal fun computeDifference(
    prevSnapshot: CompilationSnapshotBase,
    curSnapshot: CompilationSnapshotBase
): SnapshotDiff {
    if (!prevSnapshot.compilerDigest.isEqualTo(curSnapshot.compilerDigest)) {
        return FullRecompilationRequired("Compiler changed")
    }

    // TODO(mlopatkin): we can probably do this in two passes instead of four.
    val commonSources = prevSnapshot.inputs intersect curSnapshot.inputs
    val removedSources = prevSnapshot.inputs subtract curSnapshot.inputs
    val addedSources = curSnapshot.inputs subtract prevSnapshot.inputs

    val (unchangedSources, changedSources) = commonSources.partition { src ->
        val currFileDigest = curSnapshot.getDigest(src)
        val prevFileDigest = prevSnapshot.getDigest(src)
        currFileDigest.isEqualTo(prevFileDigest)
    }

    if (changedSources.isEmpty() && removedSources.isEmpty() && addedSources.isEmpty()) {
        return NoChanges
    }

    return ChangedSources(unchangedSources, changedSources, addedSources, removedSources)
}
