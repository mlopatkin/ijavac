package name.mlopatkin.ijavac.core.snapshot

import name.mlopatkin.ijavac.core.makeLogger
import java.io.File
import java.io.IOException

private val logger = makeLogger()

/**
 * The compilation info storage. It stores various information about compilation invocation and inter-source
 * dependencies that can be used to perform incremental compilation. This file is used for both input and output. If the
 * file exists when compilation starts then its content is loaded and used to determine what needs to be recompiled.
 * The file is updated with a new information after a successful compilation. The instance of the SnapshotFile is
 * logically tied to the single "module" - a group of Java files that are always compiled together and can have
 * dependencies on each other. It makes no sense to have one "global" snapshot file because each compilation of the
 * unrelated modules will overwrite snapshots of others and cause full rebuild.
 *
 * @constructor Creates a SnapshotFile that loads/stores information in the specified file. If the file doesn't exist it
 * is created after a first successful compilation.
 *
 * @param location the file to load/store compilation information
 */
class SnapshotFile(private val location: File) {
    internal fun loadPreviousSnapshot(): PostCompilationSnapshot {
        if (!location.exists()) {
            logger.debug("Snapshot file does not exist at {}, starting from scratch", location.absolutePath)
            return emptySnapshot()
        }
        try {
            location.inputStream().use { fis ->
                fis.buffered().use { input ->
                    return loadSnapshot(input)
                }
            }
        } catch (e: IOException) {
            // TODO(mlopatkin): It is also possible to abort compilation here. Is a command-line toggle for this
            //  potentially useful?
            logger.error("Failed to load snapshot file at {}, starting from scratch", location.absolutePath, e)
            return emptySnapshot()
        }
    }

    // TODO(mlopatkin) implement safer file writes
    internal fun saveCurrentSnapshot(snapshot: PostCompilationSnapshot) {
        location.outputStream().use { fos ->
            fos.buffered().use { output ->
                snapshot.saveTo(output)
            }
        }
    }

    fun removeSnapshot() {
        if (location.exists() && !location.delete()) {
            throw IOException("Failed to delete snapshot file at $location")
        }
    }
}
