package name.mlopatkin.ijavac.core.snapshot

import name.mlopatkin.ijavac.core.SourceFile
import name.mlopatkin.ijavac.core.compiledClass
import name.mlopatkin.ijavac.core.source
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.Collections.singletonMap

internal class CompilationSnapshotTest {
    private val fooSrc = source("/tmp/Foo.java")
    private val barSrc = source("/tmp/Bar.java")

    private val fooClass = compiledClass("Foo", fooSrc, "Foo.class")
    private val barClass = compiledClass("Bar", barSrc, "Bar.class")
    private val bar1Class = compiledClass("Bar$1", barSrc, "Bar$1.class")

    @Test
    internal fun `applyCompilationInformation appends outputs and deps`() {
        val sources = listOf(fooSrc, barSrc)
        val outputs = listOf(fooClass, barClass, bar1Class)
        val deps = singletonMap(barSrc, listOf(fooSrc))

        val preSnapshot = takeFilenameBasedSnapshot(sources)
        val postSnapshot = preSnapshot.applyCompilationInformation(outputs, deps, sourcesWithStarImports = emptyList())

        assertThat(postSnapshot.getOutputs(fooSrc)).containsExactlyInAnyOrder(fooClass)
        assertThat(postSnapshot.getOutputs(barSrc)).containsExactlyInAnyOrder(barClass, bar1Class)
        assertThat(postSnapshot.getAllOutputs()).containsExactlyInAnyOrder(fooClass, barClass, bar1Class)

        assertThat(postSnapshot.getDependents(fooSrc)).containsExactlyInAnyOrder(barSrc)
        assertThat(postSnapshot.getDependents(barSrc)).isEmpty()
    }

    @Test
    internal fun `snapshot serialization can be deserialized`() {
        val sources = listOf(fooSrc, barSrc)
        val outputs = listOf(fooClass, barClass, bar1Class)
        val deps = singletonMap(barSrc, listOf(fooSrc))

        val snapshot = takeFilenameBasedSnapshot(sources).applyCompilationInformation(
            outputs, deps,
            sourcesWithStarImports = emptyList()
        )
        val restoredSnapshot = snapshot.saveAndRestore()

        assertThat(restoredSnapshot.compilerDigest).isEqualTo(snapshot.compilerDigest)
        assertThat(restoredSnapshot.inputs).isEqualTo(snapshot.inputs)
        assertThat(restoredSnapshot.getAllOutputs()).isEqualTo(snapshot.getAllOutputs())
        for (src in sources) {
            assertThat(restoredSnapshot.getDigest(src)).isEqualTo(snapshot.getDigest(src))
            assertThat(restoredSnapshot.getOutputs(src)).isEqualTo(snapshot.getOutputs(src))
        }

        assertThat(restoredSnapshot.getDependents(fooSrc)).containsExactlyInAnyOrder(barSrc)
        assertThat(restoredSnapshot.getDependents(barSrc)).isEmpty()
    }

    @Test
    internal fun `serialize sources with relative path`() {
        val sources = listOf(source("Foo.java"), source("Bar.java"))

        val snapshot =
            takeFilenameBasedSnapshot(sources).applyCompilationInformation(
                outputs = emptyList(),
                deps = emptyMap(),
                sourcesWithStarImports = emptyList()
            )
        val restoredSnapshot = snapshot.saveAndRestore()

        assertThat(restoredSnapshot.inputs).isEqualTo(snapshot.inputs)
    }

    @Test
    internal fun `applyPartialCompilationInformation copies outputs and deps from prev snapshot`() {
        val sources = listOf(fooSrc, barSrc)
        val outputs = listOf(fooClass, barClass, bar1Class)
        val deps = singletonMap(barSrc, listOf(fooSrc))

        val prevSnapshot = takeFilenameBasedSnapshot(sources).applyCompilationInformation(
            outputs, deps,
            sourcesWithStarImports = emptyList()
        )
        val updatedSnapshot = takeFilenameBasedSnapshot(sources).applyPartialCompilationInformation(
            partialOutputs = emptyList(), partialDeps = emptyMap(), previousCompilationSnapshot = prevSnapshot,
            partialSourcesWithStarImports = emptyList()
        )

        assertThat(updatedSnapshot.getOutputs(barSrc)).containsExactlyInAnyOrder(barClass, bar1Class)
        assertThat(updatedSnapshot.getOutputs(fooSrc)).containsExactlyInAnyOrder(fooClass)

        assertThat(updatedSnapshot.getDependents(fooSrc)).containsExactlyInAnyOrder(barSrc)
        assertThat(updatedSnapshot.getDependents(barSrc)).isEmpty()
    }

    @Test
    internal fun `applyPartialCompilationInformation keeps entries for which only prev snapshot has information`() {
        val sources = listOf(fooSrc, barSrc)
        val outputs = listOf(fooClass, barClass)
        val deps = singletonMap(barSrc, listOf(fooSrc))

        val prevSnapshot = takeFilenameBasedSnapshot(sources).applyCompilationInformation(
            outputs,
            deps,
            sourcesWithStarImports = emptyList()
        )
        val newPartialOutputs = listOf(fooClass)
        val newPartialDeps = singletonMap(fooSrc, emptyList<SourceFile>())
        val updatedSnapshot = takeFilenameBasedSnapshot(sources).applyPartialCompilationInformation(
            newPartialOutputs, newPartialDeps, partialSourcesWithStarImports = emptyList(), prevSnapshot,
        )


        assertThat(updatedSnapshot.getOutputs(barSrc)).containsExactlyInAnyOrder(barClass)
        assertThat(updatedSnapshot.getOutputs(fooSrc)).containsExactlyInAnyOrder(fooClass)

        assertThat(updatedSnapshot.getDependents(fooSrc)).containsExactlyInAnyOrder(barSrc)
        assertThat(updatedSnapshot.getDependents(barSrc)).isEmpty()
    }

    @Test
    internal fun `applyPartialCompilationInformation doesn't add removed files`() {
        val prevSources = listOf(fooSrc, barSrc)
        val prevOutputs = listOf(fooClass, barClass)
        val prevDeps = singletonMap(barSrc, listOf(fooSrc))
        val prevSnapshot = takeFilenameBasedSnapshot(prevSources).applyCompilationInformation(
            prevOutputs, prevDeps,
            sourcesWithStarImports = emptyList()
        )

        val newSources = listOf(fooSrc)
        val updatedSnapshot = takeFilenameBasedSnapshot(newSources).applyPartialCompilationInformation(
            partialOutputs = listOf(fooClass),
            partialDeps = emptyMap(),
            partialSourcesWithStarImports = emptyList(),
            previousCompilationSnapshot = prevSnapshot
        )

        assertThat(updatedSnapshot.inputs).containsExactlyInAnyOrder(fooSrc)
        assertThat(updatedSnapshot.getDependents(fooSrc)).isEmpty()
    }

    @Test
    internal fun `getDependents handles circular dependencies`() {
        val bazSrc = source("tmp/Baz.java")
        val bazClass = compiledClass("Baz", bazSrc, "Baz.class")
        val sources = listOf(fooSrc, barSrc, bazSrc)
        val outputs = listOf(fooClass, barClass, bazClass)
        val deps = mapOf(
            barSrc to listOf(fooSrc),
            fooSrc to listOf(bazSrc),
            bazSrc to listOf(barSrc)
        )

        val snapshot = takeFilenameBasedSnapshot(sources).applyCompilationInformation(
            outputs,
            deps,
            sourcesWithStarImports = emptyList()
        )

        assertThat(snapshot.getDependents(fooSrc)).containsExactlyInAnyOrder(barSrc, bazSrc)
        assertThat(snapshot.getDependents(barSrc)).containsExactlyInAnyOrder(fooSrc, bazSrc)
        assertThat(snapshot.getDependents(bazSrc)).containsExactlyInAnyOrder(fooSrc, barSrc)

        assertThat(snapshot.getDependents(fooSrc, barSrc)).containsExactlyInAnyOrder(bazSrc)
        assertThat(snapshot.getDependents(fooSrc, barSrc, bazSrc)).isEmpty()
    }

    @Test
    internal fun `getDependents collects transitive dependencies`() {
        val bazSrc = source("tmp/Baz.java")
        val bazClass = compiledClass("Baz", bazSrc, "Baz.class")
        val sources = listOf(fooSrc, barSrc, bazSrc)
        val outputs = listOf(fooClass, barClass, bazClass)
        val deps = mapOf(
            bazSrc to listOf(barSrc),
            barSrc to listOf(fooSrc)
        )

        val snapshot = takeFilenameBasedSnapshot(sources).applyCompilationInformation(
            outputs,
            deps,
            sourcesWithStarImports = emptyList()
        )

        assertThat(snapshot.getDependents(fooSrc)).containsExactlyInAnyOrder(barSrc, bazSrc)
        assertThat(snapshot.getDependents(barSrc)).containsExactlyInAnyOrder(bazSrc)
        assertThat(snapshot.getDependents(bazSrc)).isEmpty()
        assertThat(snapshot.getDependents(barSrc, bazSrc)).isEmpty()
        assertThat(snapshot.getDependents(fooSrc, barSrc)).containsExactlyInAnyOrder(bazSrc)
        assertThat(snapshot.getDependents(fooSrc, bazSrc)).containsExactlyInAnyOrder(barSrc)
    }

    @Test
    internal fun `getDependentsTo receive same collection`() {
        val bazSrc = source("tmp/Baz.java")
        val bazClass = compiledClass("Baz", bazSrc, "Baz.class")
        val sources = listOf(fooSrc, barSrc, bazSrc)
        val outputs = listOf(fooClass, barClass, bazClass)
        val deps = mapOf(
            bazSrc to listOf(barSrc),
            barSrc to listOf(fooSrc)
        )

        val snapshot = takeFilenameBasedSnapshot(sources).applyCompilationInformation(
            outputs,
            deps,
            sourcesWithStarImports = emptyList()
        )

        val inputsDestination = hashSetOf(barSrc)
        snapshot.getDependentsTo(inputsDestination, inputsDestination)
        assertThat(inputsDestination).containsExactlyInAnyOrder(barSrc, bazSrc)
    }

    private fun PostCompilationSnapshot.saveAndRestore(): PostCompilationSnapshot {
        val baos = ByteArrayOutputStream()
        this.saveTo(baos)
        return loadSnapshot(ByteArrayInputStream(baos.toByteArray()))
    }

    private fun PostCompilationSnapshot.getDependents(vararg sources: SourceFile): Collection<SourceFile> {
        return getDependentsTo(ArrayList(), sources.asList())
    }
}
