package name.mlopatkin.ijavac.core.snapshot

import name.mlopatkin.ijavac.core.Compiler
import name.mlopatkin.ijavac.core.SourceFile

internal typealias SourceFileDigestMap = LinkedHashMap<SourceFile, Digest<SourceFile>>

internal fun hashcodeDigest(s: SourceFile): Digest<SourceFile> {
    return Digest.createForJavaObject(s)
}

internal fun sourceList(block: SourceFileDigestMap.() -> Unit): SourceFileDigestMap {
    val map = LinkedHashMap<SourceFile, Digest<SourceFile>>()
    map.block()
    return map
}

internal fun SourceFileDigestMap.sourceFile(path: String, digest: Long = 0) {
    this[name.mlopatkin.ijavac.core.source(path)] =
        if (digest != 0L) Digest.createForTesting(digest) else Digest.createForJavaObject(path)
}

internal fun SourceFileDigestMap.toSnapshot(): PreCompilationSnapshot {
    return takeSnapshotOfInputs(
        inputs = this.keys,
        compilerDigest = Digest.createForTesting(1),
        fileDigestFunc = this::getValue
    )
}

internal fun takeFilenameBasedSnapshot(
    inputs: Collection<SourceFile>,
    compilerDigest: Digest<Compiler> = Digest.createForTesting(1)
): PreCompilationSnapshot {
    return takeSnapshotOfInputs(inputs, compilerDigest, fileDigestFunc = ::hashcodeDigest)
}
