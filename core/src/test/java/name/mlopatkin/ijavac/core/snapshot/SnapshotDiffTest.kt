package name.mlopatkin.ijavac.core.snapshot

import name.mlopatkin.ijavac.core.Compiler
import name.mlopatkin.ijavac.core.source
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class SnapshotDiffTest {
    @Test
    internal fun `Unchanged inputs require no recompilation`() {
        val sourceFiles = listOf(source("/tmp/Foo.java"))
        val compiler = Digest.createForTesting<Compiler>(1)
        val prevSnapshot = takeFilenameBasedSnapshot(sourceFiles, compiler)
        val newSnapshot = takeFilenameBasedSnapshot(sourceFiles, compiler)

        Assertions.assertThat(computeDifference(prevSnapshot, newSnapshot)).isEqualTo(NoChanges)
    }

    @Test
    internal fun `Change of compiler requires full recompilation`() {
        val sourceFiles = listOf(source("/tmp/Foo.java"))
        val oldCompiler = Digest.createForTesting<Compiler>(1)
        val newCompiler = Digest.createForTesting<Compiler>(2)

        val prevSnapshot = takeFilenameBasedSnapshot(sourceFiles, oldCompiler)
        val newSnapshot = takeFilenameBasedSnapshot(sourceFiles, newCompiler)

        Assertions.assertThat(computeDifference(prevSnapshot, newSnapshot))
            .isInstanceOf(FullRecompilationRequired::class.java)
    }

    @Test
    internal fun `Detect added file`() {
        val prevFiles = sourceList {
            sourceFile("/tmp/Foo.java")
        }

        val newFiles = sourceList {
            sourceFile("/tmp/Foo.java")
            sourceFile("/tmp/Bar.java")
        }

        val prevSnapshot = prevFiles.toSnapshot()
        val newSnapshot = newFiles.toSnapshot()

        val diff = computeDifference(prevSnapshot, newSnapshot) as ChangedSources
        Assertions.assertThat(diff.unchangedSources).containsExactlyInAnyOrder(source("/tmp/Foo.java"))
        Assertions.assertThat(diff.addedSources).containsExactlyInAnyOrder(source("/tmp/Bar.java"))
        Assertions.assertThat(diff.changedSources).isEmpty()
        Assertions.assertThat(diff.removedSources).isEmpty()
    }

    @Test
    internal fun `Detect removed file`() {
        val prevFiles = sourceList {
            sourceFile("/tmp/Foo.java")
            sourceFile("/tmp/Bar.java")
        }

        val newFiles = sourceList {
            sourceFile("/tmp/Foo.java")
        }

        val prevSnapshot = prevFiles.toSnapshot()
        val newSnapshot = newFiles.toSnapshot()

        val diff = computeDifference(prevSnapshot, newSnapshot) as ChangedSources
        Assertions.assertThat(diff.unchangedSources).containsExactlyInAnyOrder(source("/tmp/Foo.java"))
        Assertions.assertThat(diff.addedSources).isEmpty()
        Assertions.assertThat(diff.changedSources).isEmpty()
        Assertions.assertThat(diff.removedSources).containsExactlyInAnyOrder(source("/tmp/Bar.java"))
    }

    @Test
    internal fun `Detect changed file`() {
        val prevFiles = sourceList {
            sourceFile("/tmp/Foo.java", 1)
            sourceFile("/tmp/Bar.java")
        }

        val newFiles = sourceList {
            sourceFile("/tmp/Foo.java", 2)
            sourceFile("/tmp/Bar.java")
        }

        val prevSnapshot = prevFiles.toSnapshot()
        val newSnapshot = newFiles.toSnapshot()

        val diff = computeDifference(prevSnapshot, newSnapshot) as ChangedSources
        Assertions.assertThat(diff.unchangedSources).containsExactlyInAnyOrder(source("/tmp/Bar.java"))
        Assertions.assertThat(diff.addedSources).isEmpty()
        Assertions.assertThat(diff.changedSources).containsExactlyInAnyOrder(source("/tmp/Foo.java"))
        Assertions.assertThat(diff.removedSources).isEmpty()
    }

    @Test
    internal fun `Detect several changed files`() {
        val prevFiles = sourceList {
            sourceFile("/tmp/Foo.java", 1)
            sourceFile("/tmp/Bar.java", 3)
            sourceFile("/tmp/Baz.java")
        }

        val newFiles = sourceList {
            sourceFile("/tmp/Bar.java", 4)
            sourceFile("/tmp/Foo.java", 2)
            sourceFile("/tmp/Baz.java")
        }

        val prevSnapshot = prevFiles.toSnapshot()
        val newSnapshot = newFiles.toSnapshot()

        val diff = computeDifference(prevSnapshot, newSnapshot) as ChangedSources
        Assertions.assertThat(diff.unchangedSources).containsExactlyInAnyOrder(source("/tmp/Baz.java"))
        Assertions.assertThat(diff.addedSources).isEmpty()
        Assertions.assertThat(diff.changedSources).containsExactlyInAnyOrder(
            source("/tmp/Foo.java"), source("/tmp/Bar.java")
        )
        Assertions.assertThat(diff.removedSources).isEmpty()
    }

    @Test
    internal fun `Detect changed, removed and added files`() {
        val prevFiles = sourceList {
            sourceFile("/tmp/Foo.java", 1)
            sourceFile("/tmp/Bar.java")

        }

        val newFiles = sourceList {
            sourceFile("/tmp/Baz.java")
            sourceFile("/tmp/Foo.java", 2)
        }

        val prevSnapshot = prevFiles.toSnapshot()
        val newSnapshot = newFiles.toSnapshot()

        val diff = computeDifference(prevSnapshot, newSnapshot) as ChangedSources
        Assertions.assertThat(diff.unchangedSources).isEmpty()
        Assertions.assertThat(diff.addedSources).containsExactlyInAnyOrder(source("/tmp/Baz.java"))
        Assertions.assertThat(diff.changedSources).containsExactlyInAnyOrder(source("/tmp/Foo.java"))
        Assertions.assertThat(diff.removedSources).containsExactlyInAnyOrder(source("/tmp/Bar.java"))
    }
}
