package name.mlopatkin.ijavac.core

import java.io.File

fun source(path: String) = SourceFile(File(path))

fun compiledClass(binaryName: String, sourceFile: String, outputPath: String): CompiledClass {
    return compiledClass(binaryName, source(sourceFile), outputPath)
}

fun compiledClass(binaryName: String, sourceFile: SourceFile, outputPath: String): CompiledClass {
    return CompiledClass(binaryName, sourceFile, RelativePath(outputPath))
}
