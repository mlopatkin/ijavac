dependencies {
    implementation(project(":core"))
    implementation(project(":core-javac"))
    implementation(Deps.assertj)

    testImplementation(testFixtures(project(":core")))

    implementation(Deps.slf4j.simple)
}
