package name.mlopatkin.ijavac.tests

/**
 * Closer provides a way to manage multiple Closeables/Autocloseables
 */
class Cleaner : AutoCloseable {
    private val closeables = ArrayList<AutoCloseable>()

    fun <T : AutoCloseable> add(closeable: T): T {
        closeables.add(closeable)
        return closeable
    }

    fun closeBecauseOfException(cause: Throwable): Nothing {
        closeInternal(cause)
        throw cause
    }

    private fun closeInternal(cause: Throwable?) {
        var pendingException: Throwable? = cause
        for (it in closeables.reversed()) {
            try {
                it.close()
            } catch (t: Throwable) {
                if (pendingException == null) {
                    pendingException = Exception("Exception when closing multiple closeables")
                }
                pendingException.addSuppressed(t)
            }
        }
        if (pendingException != null) {
            throw pendingException
        }
    }

    override fun close() {
        closeInternal(null)
    }
}
