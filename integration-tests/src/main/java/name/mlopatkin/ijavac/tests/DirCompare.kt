package name.mlopatkin.ijavac.tests

import org.assertj.core.api.AbstractAssert
import java.io.File

fun assertThatDir(dir: File): DirCompare {
    return DirCompare(dir)
}

class DirCompare(actual: File) : AbstractAssert<DirCompare, File>(actual, DirCompare::class.java) {
    fun hasTheSameContentAs(expectedDir: File) {
        val expectedContent = expectedDir.listChildrenFiles().toSortedSet()
        val actualContent = actual.listChildrenFiles().toSortedSet()

        val expectedFileNames = expectedContent.map { it.toRelativeString(expectedDir) }.toSortedSet()
        val actualFileNames = actualContent.map { it.toRelativeString(actual) }.toSortedSet()

        if (expectedFileNames != actualFileNames) {
            val extraFiles = actualFileNames subtract expectedFileNames
            val missingFiles = expectedFileNames subtract actualFileNames
            throw failure(
                "extra files = (${extraFiles.joinToString()}), " +
                        "missing files = (${missingFiles.joinToString()})"
            )
        }

        val differentFiles = mutableSetOf<File>()
        expectedContent.asSequence().zip(actualContent.asSequence()).forEach { (expectedFile, actualFile) ->
            if (!compareFiles(expectedFile, actualFile)) {
                differentFiles.add(actualFile)
            }
        }
    }

    private fun compareFiles(expectedFile: File, actualFile: File): Boolean {
        val expected = expectedFile.readBytes()
        val actual = actualFile.readBytes()
        return expected.contentEquals(actual)
    }
}

