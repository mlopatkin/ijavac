package name.mlopatkin.ijavac.tests

import java.io.File
import java.io.IOException

/**
 * Lists all files in the given directory and its subdirectories.
 */
fun File.listChildrenFiles(): Sequence<File> {
    return walkTopDown().filterNot { it.isDirectory }
}

/**
 * Deletes all children of the directory.
 */
fun File.deleteChildren() {
    if (!isDirectory) {
        throw IllegalArgumentException("Cannot delete children of a regular file $this")
    }
    listFiles()?.forEach {
        if (!it.deleteRecursively()) {
            throw IOException("Failed to delete $it")
        }
    }
}
