package name.mlopatkin.ijavac.tests

import name.mlopatkin.ijavac.core.CompileSuccess
import name.mlopatkin.ijavac.core.Compiler
import name.mlopatkin.ijavac.core.IncrementalCompilationResult
import name.mlopatkin.ijavac.core.IncrementalCompiler
import name.mlopatkin.ijavac.core.ScopedTempDir
import name.mlopatkin.ijavac.core.SourceFile
import name.mlopatkin.ijavac.core.javac.JavacCompiler
import name.mlopatkin.ijavac.core.snapshot.SnapshotFile
import org.assertj.core.api.Assertions.assertThat
import java.io.Closeable
import java.io.File
import java.nio.file.Files
import javax.tools.ToolProvider

// TODO(mlopatkin): this is a lame way to find test sources :(
private const val RESOURCE_BASE_DIR = "/home/mlopatkin/ijavac/integration-tests/src/test/testcases"
private const val BASE_FILES_DIR_NAME = "_"

class IntegrationTestHarness private constructor(private val resourceDir: File) : Closeable {
    private val harnessCleaner = Cleaner()

    private val inputDir: File
    private val incrementalOutputDir: File
    private val nonIncrementalOutputDir: File
    private val miscDir: File

    init {
        try {
            inputDir = harnessCleaner.add(ScopedTempDir(prefix = "ijavacInput")).dir
            incrementalOutputDir = harnessCleaner.add(ScopedTempDir(prefix = "ijavacIncrementalOutput")).dir
            nonIncrementalOutputDir = harnessCleaner.add(ScopedTempDir(prefix = "ijavacNonIncrementalOutput")).dir
            miscDir = harnessCleaner.add(ScopedTempDir(prefix = "ijavacMisc")).dir

            setUpBaseFiles(File(resourceDir, BASE_FILES_DIR_NAME), inputDir)
        } catch (t: Throwable) {
            harnessCleaner.closeBecauseOfException(t)
        }
    }

    private val snapshotFile = File(miscDir, "snapshot.bin")

    fun performCompilation(
        testCase: String,
        compareOutputs: Boolean = true
    ): IncrementalCompilationResult {
        Cleaner().use { invocationCleaner ->
            val caseSpecificFiles = resourceDir.resolve(testCase).copyToInputDir()
            invocationCleaner.deleteOnCleanup(caseSpecificFiles)
            if (compareOutputs) {
                invocationCleaner.add(Closeable { nonIncrementalOutputDir.deleteChildren() })
            }
            val incrementalResult = Cleaner().use { cleaner ->
                val compiler = buildCompiler(cleaner)

                val incrementalCompiler =
                    IncrementalCompiler(incrementalOutputDir, compiler, SnapshotFile(snapshotFile))
                incrementalCompiler.compile(inputDir.listSources())
            }

            val nonIncrementalResult = if (compareOutputs) {
                Cleaner().use { cleaner ->
                    val compiler = buildCompiler(cleaner)

                    compiler.compile(inputDir.listSources(), nonIncrementalOutputDir)
                }
            } else {
                null
            }
            if (nonIncrementalResult != null) {
                val isNonIncrementalSuccess = nonIncrementalResult is CompileSuccess
                if (isNonIncrementalSuccess) {
                    assertThat(incrementalResult.success)
                        .withFailMessage("Expecting incremental compilation to succeed")
                        .isTrue()
                } else {
                    assertThat(incrementalResult.success)
                        .withFailMessage("Expecting incremental compilation to fail")
                        .isFalse()
                }
            }
            if (incrementalResult.success && nonIncrementalResult is CompileSuccess) {
                assertThatDir(incrementalOutputDir).hasTheSameContentAs(nonIncrementalOutputDir)
            }
            return incrementalResult
        }
    }

    private fun File.copyToInputDir(): List<File> {
        if (!exists()) {
            return emptyList()
        }
        val copiedFilesAndDirs = ArrayList<File>()
        for (f in this.walkTopDown()) {
            val target = inputDir.resolve(f.relativeTo(this))
            if (!(f.isDirectory && f.exists())) {
                // Skip existing directories, they were probably created during the base setup. Existing files are fine,
                // copyTo will happily throw.
                f.copyTo(target, overwrite = false)
                copiedFilesAndDirs.add(target)
            }
        }
        copiedFilesAndDirs.reverse()
        return copiedFilesAndDirs
    }

    private fun List<File>.deleteAll() {
        forEach { file ->
            Files.delete(file.toPath())
        }
    }

    private fun Cleaner.deleteOnCleanup(files: List<File>) {
        add(Closeable { files.deleteAll() })
    }

    private fun File.listSources(): List<SourceFile> {
        return listChildrenFiles().sorted().map { SourceFile(it) }.toList()
    }

    private fun buildCompiler(cleaner: Cleaner): Compiler {
        val realCompiler = ToolProvider.getSystemJavaCompiler()
        val fileManager = cleaner.add(realCompiler.getStandardFileManager(null, null, null))
        return cleaner.add(JavacCompiler(realCompiler, fileManager))
    }

    override fun close() {
        harnessCleaner.close()
    }

    companion object {
        fun fromResources(resourceDirName: String): IntegrationTestHarness {
            val resourceDir = File(RESOURCE_BASE_DIR, resourceDirName)
            if (!resourceDir.isDirectory) {
                throw IllegalArgumentException("Directory $resourceDir is not a directory or doesn't exist")
            }
            return IntegrationTestHarness(resourceDir)
        }

        private fun setUpBaseFiles(baseDir: File, targetDir: File) {
            if (baseDir.exists()) {
                baseDir.copyRecursively(targetDir, overwrite = false)
            }
        }

        init {
            System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "warn")
        }
    }
}
