package name.mlopatkin.ijavac.tests

import name.mlopatkin.ijavac.core.IncrementalCompilationResult
import name.mlopatkin.ijavac.core.NonIncrementalReason
import name.mlopatkin.ijavac.core.SourceFile
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

@Suppress("UsePropertyAccessSyntax")
class IntegrationTest {
    @Test
    fun `01-single-file`() {
        IntegrationTestHarness.fromResources("01-single-file").use { harness ->
            val initialResult = harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(initialResult.reason).isEqualTo(NonIncrementalReason.INCOMPATIBLE_CHANGES)
            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("A.java")
        }
    }

    @Test
    fun `02-multiple-files`() {
        IntegrationTestHarness.fromResources("02-multiple-files").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("A.java", "B.java")
        }
    }

    @Test
    fun `03-multiple-files-one-changed`() {
        IntegrationTestHarness.fromResources("03-multiple-files-one-changed").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("B.java")
        }
    }

    @Test
    fun `04-multiple-files-one-removed`() {
        IntegrationTestHarness.fromResources("04-multiple-files-one-removed").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).isEmpty()
        }
    }

    @Test
    fun `05-multiple-files-one-added`() {
        IntegrationTestHarness.fromResources("05-multiple-files-one-added").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("C.java")
        }
    }

    @Test
    fun `06-duplicate-class-appears`() {
        IntegrationTestHarness.fromResources("06-duplicate-class-appears").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            harness.performCompilation("1") as IncrementalCompilationResult.Failure
        }
    }

    @Test
    fun `07-class-moves-from-one-file-to-other`() {
        IntegrationTestHarness.fromResources("07-class-moves-from-one-file-to-other").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            harness.performCompilation("1") as IncrementalCompilationResult.Success
        }
    }

    @Test
    fun `08-dependency-on-unchanged-file`() {
        IntegrationTestHarness.fromResources("08-dependency-on-unchanged-file").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("B.java")
        }
    }

    @Test
    fun `09-changed-dependency-of-unchanged-file`() {
        IntegrationTestHarness.fromResources("09-changed-dependency-of-unchanged-file").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("A.java", "B.java")
        }
    }

    @Test
    fun `10-different-ways-to-depend-on-file`() {
        IntegrationTestHarness.fromResources("10-different-ways-to-depend-on-file").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder(
                "Dependency.java",

                "CallsStaticViaAncestor.java",
                "Extends.java",
                "Implements.java",
                "UseAnnotation.java",
                "UseAsFieldType.java",
                "UseAsGenericBound.java",
                "UseAsGenericBoundOfArgument.java",
                "UseAsGenericParameter.java",
                "UseClassLiteral.java",
                "UseStaticMethod.java",
                "UseStaticMethodRef.java",
            )
        }
    }

    @Test
    fun `11-packages`() {
        IntegrationTestHarness.fromResources("11-packages").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder("C.java")
        }
    }

    @Test
    fun `12-static-imports`() {
        IntegrationTestHarness.fromResources("12-static-imports").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder(
                "A.java",
                "Main.java"
            )
        }
    }

    @Test
    fun `13-static-import-replaces-method`() {
        IntegrationTestHarness.fromResources("13-static-import-replaces-method").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder(
                "A.java",
                "Main.java"
            )
        }
    }

    @Test
    fun `14-static-star-import-replaces-method`() {
        IntegrationTestHarness.fromResources("14-static-star-import-replaces-method").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder(
                "A.java",
                "Main.java"
            )
        }
    }

    @Test
    fun `15-star-import-replaces-things`() {
        IntegrationTestHarness.fromResources("15-star-import-replaces-things").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            assertThat(harness.performCompilation("1") is IncrementalCompilationResult.Failure).isTrue()
        }
    }

    @Test
    fun `16-indirect-access-to-the-type`() {
        IntegrationTestHarness.fromResources("16-indirect-access-to-the-type").use { harness ->
            harness.performCompilation("0") as IncrementalCompilationResult.NonIncrementalSuccess
            val recompilationResult = harness.performCompilation("1") as IncrementalCompilationResult.Success

            assertThat(recompilationResult.recompiledInputs.toFileNames()).containsExactlyInAnyOrder(
                "A.java",
                "B.java",
                "C.java",
                "D.java",
            )
        }
    }

    fun Collection<SourceFile>.toFileNames(): Collection<String> {
        return this.map { File(it.file).name }
    }
}
