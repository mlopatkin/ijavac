public class Dependency {
    public @interface Annotation {
    }

    public interface SubInterface {
    }

    public static int getFoo() {
        return 1;
    }

    public void makeBar() {
    }
}
