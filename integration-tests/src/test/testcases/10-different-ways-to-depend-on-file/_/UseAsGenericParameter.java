class UseAsGenericParameter<T> {
    void foo() {
        new UseAsGenericParameter<Dependency>();
    }
}
