import org.gradle.internal.jvm.Jvm

plugins {
    id("com.github.johnrengelman.shadow")
}

dependencies {
    if (JavaVersion.current() == JavaVersion.VERSION_1_8) {
        compileOnly(files(Jvm.current().toolsJar!!))
    }

    compileOnly(project(":core"))
}
