package name.mlopatkin.ijavac.javacplugin

import com.sun.source.util.JavacTask
import com.sun.source.util.Plugin
import name.mlopatkin.ijavac.core.ContextManager
import javax.tools.JavaFileObject

typealias Context = ContextManager.Context<JavaFileObject>

@Suppress("unused")
class CompileInfoPlugin : Plugin {
    override fun getName() = "name.mlopatkin.ijavac.javacplugin.CompileInfoPlugin"

    override fun init(task: JavacTask, vararg args: String) {
        val context : Context = ContextManager.findContext(ContextManager.key(task))

        task.addTaskListener(InputRecorder(context, task))
        task.addTaskListener(DependencyRecorder(context, task))
    }
}
