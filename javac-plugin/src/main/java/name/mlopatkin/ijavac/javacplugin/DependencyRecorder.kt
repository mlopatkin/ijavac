package name.mlopatkin.ijavac.javacplugin

import com.sun.source.tree.ClassTree
import com.sun.source.tree.ImportTree
import com.sun.source.tree.MemberSelectTree
import com.sun.source.tree.Tree
import com.sun.source.util.JavacTask
import com.sun.source.util.TaskEvent
import com.sun.source.util.TreePath
import com.sun.source.util.TreePathScanner
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement

/**
 * Collects all dependencies of the compiled source files.
 */
internal class DependencyRecorder(context: Context, task: JavacTask) : TaskBase(task) {
    private val context = context

    override fun finished(event: TaskEvent) {
        if (event.kind != TaskEvent.Kind.ANALYZE) return

        val seenTypes = HashSet<String>()
        val treeVisitor = object : TreePathScanner<Void?, Void?>() {
            override fun scan(tree: Tree?, ignored: Void?): Void? {
                if (tree != null && tree !is ClassTree) {
                    val typeName = when (val element = TreePath(currentPath, tree).element) {
                        is TypeElement -> element.binaryName
                        is ExecutableElement -> (element.enclosingElement as? TypeElement).binaryName
                        is VariableElement -> (element.enclosingElement as? TypeElement).binaryName
                        else -> null
                    }
                    if (typeName != null) {
                        seenTypes.add(typeName.toString())
                    }
                }
                if (tree.isStarImport()) {
                    context.recordStarInput(event.sourceFile)
                }
                return super.scan(tree, ignored)
            }
        }
        treeVisitor.scan(event.compilationUnit, null)
        context.recordDependencies(event.sourceFile, seenTypes)
    }

    private fun Tree?.isStarImport(): Boolean {
        if (this is ImportTree && !isStatic) {
            when (val identifierChain = qualifiedIdentifier) {
                is MemberSelectTree -> {
                    return identifierChain.identifier.toString() == "*"
                }
            }
        }
        return false
    }
}
