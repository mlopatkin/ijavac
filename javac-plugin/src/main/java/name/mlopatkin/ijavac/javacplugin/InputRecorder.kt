package name.mlopatkin.ijavac.javacplugin

import com.sun.source.util.JavacTask
import com.sun.source.util.TaskEvent

internal class InputRecorder(context: Context, task: JavacTask) : TaskBase(task) {
    private val context = context

    override fun finished(event: TaskEvent) {
        if (event.kind != TaskEvent.Kind.GENERATE) return
        context.recordInput(event.typeElement.binaryName.toString(), event.sourceFile)
    }
}
