package name.mlopatkin.ijavac.javacplugin

import com.sun.source.util.JavacTask
import com.sun.source.util.TaskEvent
import com.sun.source.util.TaskListener
import com.sun.source.util.TreePath
import com.sun.source.util.Trees
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement

/**
 * Abstract base class for task listeners that provides some utility extension methods.
 */
internal abstract class TaskBase(task: JavacTask) : TaskListener {
    private val elements = task.elements
    private val trees = Trees.instance(task)

    override fun started(event: TaskEvent) {
    }

    override fun finished(event: TaskEvent) {
    }

    val TypeElement?.binaryName: CharSequence?
        get() {
            return if (this != null) {
                elements.getBinaryName(this)
            } else {
                null
            }
        }

    val TreePath.element: Element?
        get() {
            return trees.getElement(this)
        }
}
