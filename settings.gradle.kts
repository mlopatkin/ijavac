rootProject.name = "ijavac"
include("core")
include("cli")
include("javac-plugin")
include("core-javac")
include("integration-tests")
